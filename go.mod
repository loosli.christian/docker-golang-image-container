module docker-golang-image-container

go 1.12

require (
	github.com/gorilla/mux v1.7.2
	gopkg.in/natefinch/lumberjack.v2 v2.0.0
)
