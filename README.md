# docker golang image container

## docker build
```bash
  docker build --rm -t go-docker-optimized .
```

## docker run
```bash
  docker run -it -d --rm -p 8080:8080 go-docker-optimized
```
